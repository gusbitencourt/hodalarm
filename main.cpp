#include <QCoreApplication>
#include <iostream>
#include <QTime>
#include <QString>
#include <QDebug>
#include <QTimer>
#include <QFile>
#include <QTextStream>
#include <QStringList>

QTime insertData()
{
    int h;
    int m;
    QTime tempTime;

    qDebug() << "Digite a hora do alarme: (Valores entre 0-23)";
    do {
        std::cin >> h;
      } while (0 > h || h > 23);
    qDebug() << "Digite o minuto do alarme: (Valores entre 0-59)";
    do {
        std::cin >> m;
    } while (0 > m || m > 59);

    tempTime = QTime(h, m, 0, 500);

    return tempTime;
}

std::vector<QTime> readFromFile()
{
    std::vector<QTime> vTxt;
    int h;
    int m;

    QFile txt("horas.txt");
    txt.open(stderr, QIODevice::ReadOnly);
    QTextStream rawTime(&txt);
    do {
        QString line = rawTime.readLine();
        QStringList list = line.split(':');
        h = list.at(0).toInt();
        m = list.at(1).toInt();
        vTxt.push_back(QTime(h, m, 0, 500));
    } while (!rawTime.atEnd());
    txt.close();

    return vTxt;
}

std::vector<QTime> alarmTime()
{
    std::vector<QTime> vT;
    int breakout = 0;

    do {
         vT.push_back(insertData());
         qDebug() << "**Aperte (1) e ENTER se quiser criar um novo alarme, (0) e ENTER se ja terminou.**";
         std::cin >> breakout;
    } while (breakout != 0);

    return vT;
}

void alarm()
{
    QString actualHour;

    actualHour = QTime::currentTime().toString();
    qDebug() << "plim plim " << actualHour << " plim plim";
    return;
}

QTime questHour()
{
    int h;
    int m;
    QTime defTime;

    qDebug() << "Que hora (0-23) é agora?";
    do {
        std::cin >> h;
    } while (0 > h || h > 23);
    qDebug() << "E os minutos? (0-59)";
    do {
        std::cin >> m;
    } while (0 > m || m > 59);

    defTime = QTime(h, m, 0, 500);

    return defTime;

}

QTime chooseTime()
{
    int horario;
    QTime chHorario;

    qDebug() << "Usa horario local (0) ou entrar com um horario (1)?";
    do {
        std::cin >> horario;
    } while(horario != 1 && horario != 0);
    if (horario == 0){
        chHorario = QTime::currentTime();
    }
    else {
        chHorario = questHour();
    }

    return chHorario;
}

void createAlarms()
{
    int choose;
    std::vector<QTime> times;

    qDebug() << "Deseja recuperar as horas dos alarmes de um arquivo (0) ou digita-las (1)?";
    do{
        std::cin >> choose;
    } while(choose != 1 && choose != 0);
    if (choose == 0){
        times = readFromFile();
    }
    else {
        times = alarmTime();
    }

    do {
        QTimer::singleShot(chooseTime().msecsTo(times.back()), alarm);
        times.pop_back();
    } while (!times.empty());
}

int main(int argc, char *argv[])
{
    QCoreApplication timer(argc, argv);

    createAlarms();

    return timer.exec();
}
